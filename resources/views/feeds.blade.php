@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">Feeds</div>

        <div class="panel-body" id="main-content">
          <div class="col-sm-12">
            <table class="table" id="feeds-table">
              <tr>
                <th>#</th><th>Title</th><th>Category</th><th>Url</th><th></th>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@include('modal.feeds')

@endsection


@section('extrajs')

<script src="{{ asset('js/feeds.js') }}"></script>
<script>

$( document ).ready(function() {
  loadFeeds();
});

</script>
@endsection
