@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">Feeds Categories</div>

        <div class="panel-body" id="main-content">
          <form>
            <div class="col-sm-2">
              <label>Title</label>
            </div>
            <div class="col-sm-3">
              <input name="add_title" id="add_title" class="form-control" placeholder="title" />
            </div>
            <div class="col-sm-2">
              <button class="btn btn-sm btn-success" id="addCategory">Submit</button>
            </div>
          </form>
          <div class="col-sm-12">
            <table class="table" id="categories-table">
              <tr>
                <th>#</th><th>Title</th><th></th>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection


@section('extrajs')
<script src="{{ asset('js/feeds.js') }}"></script>
<script>

$( document ).ready(function() {
  loadCategories();

  $( "#addCategory" ).click(function() {
    addCategory();
  });

});


</script>
@endsection
