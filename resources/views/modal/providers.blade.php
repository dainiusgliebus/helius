<div class="modal fade" id="providerModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="providerModalTitle">Modal title</h4>
      </div>
      <div class="modal-body" id="providerModalBody">
        <form>
          <div class="col-sm-2">
            <label>Title</label>
          </div>
          <div class="col-sm-10">
            <input name="edit_title" id="edit_title" class="form-control" placeholder="title" />
          </div>
          <div class="col-sm-2">
            <label>Feed url</label>
          </div>
          <div class="col-sm-10">
            <input name="edit_url" id="edit_url" class="form-control" placeholder="feed url" />
          </div>
          <div class="col-sm-2">
            <label>Category</label>
          </div>
          <div class="col-sm-10">
            <select id="category-filter-edit" class="form-control">
              <option value="">Filter By Cateogry</option>
            </select>
          </div>
          <input type="hidden" name="edit_id" id="edit_id" />
        </form>
        <br />
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="providerUpdate">Update</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
