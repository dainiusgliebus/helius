@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">Feeds Providers</div>

        <div class="panel-body" id="main-content">
          <div class="col-sm-12">
            <div class="row">
              <form>
                <div class="col-sm-2 col-lg-1">
                  <label>Title</label>
                </div>
                <div class="col-sm-3 col-lg-2">
                  <input name="add_title" id="add_title" class="form-control" placeholder="title" />
                </div>
                <div class="col-sm-2 col-lg-1">
                  <label>Url</label>
                </div>
                <div class="col-sm-3 col-lg-3">
                  <input name="add_url" id="add_url" class="form-control" placeholder="feed url" />
                </div>
                <div class="col-sm-2 col-lg-1">
                  <label>Category</label>
                </div>
                <div class="col-sm-3 col-lg-3">
                  <select id="add_category" class="form-control">
                    <option value="">Cateogry</option>
                  </select>
                </div>
                <div class="col-sm-1 col-lg-1">
                  <button class="btn btn-sm btn-success" id="addProvider">Submit</button>
                </div>
              </form>
            </div>
          </div>

          <div class="col-sm-5" style="margin-top:20px">
              <label>Filter</label>
              <select id="category-filter" class="form-control">
                <option value="">All</option>
              </select>
          </div>
          <div class="col-sm-12" style="margin-top:20px">
            <table class="table" id="providers-table">
              <tr>
                <th>#</th><th>Title</th><th>Category</th><th>Url</th><th></th><th></th>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@include('modal.providers')

@endsection


@section('extrajs')
<script src="{{ asset('js/feeds.js') }}"></script>
<script>

var filterCategory = null;

$( document ).ready(function() {
  loadProviders();
  loadCategorySelect();

  $( "#addProvider" ).click(function() {
    addProvider();
  });

  $( "#providerUpdate" ).click(function() {
    providerUpdate();
  });

  $('#category-filter').change(function() {
    filterCategory = $(this).val();
    loadProviders();
  });

});


</script>
@endsection
