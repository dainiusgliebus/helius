function log(data){
  console.log(data);
}

function clearTable(id){
  $('#' + id + ' tr').slice(1).remove();
}

function removeRow(id) {
  var tr = document.getElementById(id);
  if (tr) {
    if (tr.nodeName == 'TR') {
      var tbl = tr; // Look up the hierarchy for TABLE
      while (tbl != document && tbl.nodeName != 'TABLE') {
        tbl = tbl.parentNode;
      }

      if (tbl && tbl.nodeName == 'TABLE') {
        while (tr.hasChildNodes()) {
          tr.removeChild( tr.lastChild );
        }
        tr.parentNode.removeChild( tr );
      }
    } else {
      alert( 'Specified document element is not a TR. id=' + id );
    }
  } else {
    alert( 'Specified document element is not found. id=' + id );
  }
}

function appendTable(id, row){
  $(id).append(row);
}

function loadProviders(){
  let extend = '';
  if(filterCategory != null){
    extend = '?category_id='+filterCategory;
  }
  jQuery.ajax({
    type: 'GET',
    url: '/api/providers'+extend,
    success: function (response) {
      if(response.message == 'success'){
        clearTable('providers-table');
        $.each(response.data, function(i, obj) {
          appendTable('#providers-table', addProviderRow(response.data[i]));
        });
      }
    }
  });
}

function loadCategories(){
  jQuery.ajax({
    type: 'GET',
    url: '/api/categories',
    success: function (response) {
      if(response.message == 'success'){
        $.each(response.data, function(i, obj) {
          appendTable('#categories-table', addCategoryRow(response.data[i]));
        });
      }
    }
  });
}

function loadFeeds(){
  url = '/api/feeds';
  if ("provider" in window) {
    url = '/api/providers/' + provider + '/feeds';
  }
  jQuery.ajax({
    type: 'GET',
    url: url,
    success: function (response) {
      if(response.message == 'success'){
        $.each(response.data, function(i, obj) {
          appendTable('#feeds-table', addFeedsRow(response.data[i]));
        });
      }
    }
  });
}

function deleteProvider(id){
  jQuery.ajax({
    type: 'DELETE',
    url: '/api/providers/'+id,
    success: function (response) {
      if(response.message == 'success'){
        removeRow('row-id-'+id);
      }
    }
  });
}

function deleteCategory(id){
  jQuery.ajax({
    type: 'DELETE',
    url: '/api/categories/'+id,
    success: function (response) {
      if(response.message == 'success'){
        removeRow('row-id-'+id);
      }
    }
  });
}

function updateProvider(id){
  jQuery.ajax({
    type: 'POST',
    url: '/api/providers/'+id,
    success: function (response) {
      if(response.message == 'success'){
        removeRow('row-id-'+id);
      }
    }
  });
}

function addCategory(){
  event.preventDefault();

  var obj = {};
  obj.title = $('#add_title').val();

  jQuery.ajax({
    type: 'PUT',
    url: '/api/categories',
    dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(obj),
    success: function (response) {
      if(response.message == 'success'){
          appendTable('#categories-table', addCategoryRow(response.data));
      }
    }
  });
}

function addProvider(){
  event.preventDefault();

  var provider = {};
  provider.title       = $('#add_title').val();
  provider.url         = $('#add_url').val();
  provider.category_id = $('#category-filter').val();

  jQuery.ajax({
    type: 'PUT',
    url: '/api/providers',
    dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(provider),
    success: function (response) {
      if(response.message == 'success'){
          appendTable('#providers-table', addProviderRow(response.data));
      }
    }
  });
}

function providerUpdate(){

  event.preventDefault();

  var providerId    = $('#edit_id').val();

  var provider = {};
  provider.title       = $('#edit_title').val();
  provider.url         = $('#edit_url').val();
  provider.category_id = $('#category-filter-edit').val();
  if(provider.category_id == ''){
    provider.category_id = null;
  }

  jQuery.ajax({
    type: 'POST',
    url: '/api/providers/'+providerId,
    dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(provider),
    success: function (response) {
      if(response.message == 'success'){
          removeRow('row-id-'+providerId);
          appendTable('#providers-table', addProviderRow(response.data));
          $('#providerModal').modal('toggle');
      }
    }
  });
}

function updateProviderModal(id){
  $('#providerModal').modal('toggle');

  jQuery.ajax({
    type: 'GET',
    url: '/api/providers/'+id,
    dataType: 'json',
    contentType: "application/json",
    success: function (response) {
      if(response.message == 'success'){
          $( "#edit_id" ).val( response.data.id );
          $( "#providerModalTitle" ).text( response.data.title );
          $( "#edit_title" ).val( response.data.title );
          $( "#edit_url" ).val( response.data.url );
          $( "#category-filter-edit").val("").change();
          if(response.data.category != null){
            $( "#category-filter-edit").val(response.data.category.id).change();
          }

      }
    }
  });
}

function loadFeed(id){
  $('#feedModal').modal('toggle');

  jQuery.ajax({
    type: 'GET',
    url: '/api/feeds/'+id,
    dataType: 'json',
    contentType: "application/json",
    success: function (response) {
      if(response.message == 'success'){
          $( "#feedModalTitle" ).text( response.data.title );
          $( "#feedModalBody" ).text( response.data.description );
          $("a#feedModalLink").attr("href", response.data.link)
      }
    }
  });
}

function addProviderRow(obj, i){
  var cat = '';
  if(obj.category != null){
    cat = obj.category.title;
  }
  var string = '<tr id="row-id-' + obj.id + '">' +
  '<td>' + obj.id + '</td>' +
  '<td id="edit_title_' + obj.id + '">' + obj.title +'</td>' +
  '<td id="edit_category_' + obj.id + '">' + cat +'</td>' +
  '<td id="edit_url_' + obj.id + '">' + obj.url +'</td>' +
  '<td><a onclick="deleteProvider(' +  obj.id + ')" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></td>' +
  '<td><a onclick="updateProviderModal(' +  obj.id + ')" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil"></i></td>' +
  '<td><a href="/providers/' +  obj.id + '/feeds" target="_blank">View Feeds</a></td></tr>';

  return $(string);
}

function addCategoryRow(obj, i){
  var string = '<tr id="row-id-' + obj.id + '"><td>' + obj.id + '</td><td>' + obj.title +'</td>' +
  '<td><a onclick="deleteCategory(' +  obj.id + ')" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></td></tr>';

  return $(string);
}

function addFeedsRow(obj, i){
  var string = '<tr id="row-id-' + obj.id + '"><td>' + obj.id + '</td>' +
  '<td><a data-id="' + obj.id + '" title="' + obj.title + '" class="open-feedDialog" data-toggle="modal" data-target="#showFeed" onclick="loadFeed(' + obj.id + ')">' + obj.title + '</a></td>' +
  '<td>' + obj.category +'</td><td>' + obj.link +'</td></tr>';

  return $(string);
}



function loadCategorySelect(){
  jQuery.ajax({
    type: 'GET',
    url: '/api/categories/',
    dataType: 'json',
    contentType: "application/json",
    success: function (response) {
      if(response.message == 'success'){
        var options = $('#add_category');
        var options2 = $('#category-filter');
        var options3 = $('#category-filter-edit');

        $.each(response.data, function(i, obj) {
          options.append($("<option />").val(obj.id).text(obj.title));
          options2.append($("<option />").val(obj.id).text(obj.title));
          options3.append($("<option />").val(obj.id).text(obj.title));
        });
      }
    }
  });
}
