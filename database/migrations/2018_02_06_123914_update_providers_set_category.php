<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProvidersSetCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('providers', function($table) {
         $table->integer('category_id')->nullable();

         $table->foreign('category_id')->references('id')->on('feeds_category');
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('providers', function($table) {
         $table->dropColumn('category_id');
      });
    }
}
