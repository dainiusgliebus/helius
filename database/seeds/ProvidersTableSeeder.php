<?php

use Illuminate\Database\Seeder;

class ProvidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = [
        [
          'title' => 'Alta',
          'url' => 'https://www.alfa.lt/rss',
        ],
        [
          'title' => 'Feed burner',
          'url' => 'http://feeds.feedburner.com/technologijos-visos-publikacijos?format=xml',
        ]
      ];
      
      DB::table('providers')->insert($data);
    }
}
