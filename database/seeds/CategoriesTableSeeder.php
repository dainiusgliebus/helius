<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = [
        ['title' => 'News'],
        ['title' => 'Sport'],
        ['title' => 'Crime']
      ];
      DB::table('feeds_category')->insert($data);
    }
}
