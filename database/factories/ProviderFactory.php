<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Provider::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'url'   => $faker->url,
    ];
});
