<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Feed::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'link'  => $faker->url,
        'description' => $faker->realText(rand(10,20)),
        'category' => $faker->title,
        'provider_id' => function () {
            return factory(\App\Models\Provider::class)->create()->id;
        }
    ];
});
