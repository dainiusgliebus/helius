<?php

namespace App\Models;

use App\Models\Relationships\ProviderRelationships;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{

    use ProviderRelationships;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'url', 'category_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'deleted_at', 'category_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $casts = [
    ];

    protected $table = 'providers';

}
