<?php

namespace App\Models\Relationships;
use App\Models\Category;
use App\Models\Feed;

trait ProviderRelationships
{

  public function feeds(){
    return $this->hasMany(Feed::class);
  }

  public function category(){
    return $this->belongsTo(Category::class);
  }
}
