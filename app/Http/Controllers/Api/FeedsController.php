<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Feed;

class FeedsController extends Controller
{

    public function list()
    {
      return $this->success(Feed::all());
    }

    public function create(Request $request)
    {
      $model = new Feed($request->all());
      $model->save();
      return $this->success($model);
    }

    public function view(int $id)
    {
      $model = Feed::find($id);
      return $this->success($model);
    }

    public function update(Request $request, int $id)
    {
      $model = Feed::find($id);
      if($model){
        $model->fill($request->all());
        $model->save();
      }
      return $this->success($model);
    }

    public function delete(int $id)
    {
      $model = Feed::find($id);
      if($model){
        $model->delete();
      }
      return $this->success();
    }

    public function categories(){
      return $this->success();
    }
}
