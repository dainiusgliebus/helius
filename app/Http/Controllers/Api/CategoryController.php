<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{

    public function list()
    {
      return $this->success(Category::all());
    }

    public function create(Request $request)
    {
      $model = new Category($request->all());
      $model->save();
      return $this->success($model);
    }

    public function view(int $id)
    {
      $model = Category::find($id);
      return $this->success($model);
    }

    public function update(Request $request, int $id)
    {
      $model = Category::find($id);
      if($model){
        $model->fill($request->all());
        $model->save();
      }
      return $this->success($model);
    }

    public function delete(int $id)
    {
      $model = Category::find($id);
      if($model){
        $model->delete();
      }
      return $this->success();
    }
}
