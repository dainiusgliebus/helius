<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Provider;
use App\Http\Requests\Feed\FeedRequest;

class ProviderController extends Controller
{

    public function list(Request $request)
    {
      if((int) $request->category_id > 0){
        $models = Provider::where('category_id', (int) $request->category_id)->get();
      }else{
        $models = Provider::all();
      }

      // preload category model for provider TODO check if its possible to do it only once
      foreach($models as $model){
        $model->category;
      }
      return $this->success($models);
    }

    public function create(Request $request)
    {
      $model = new Provider($request->all());
      $model->save();
      // preload category model for provider
      $model->category;
      return $this->success($model);
    }

    public function view(int $id)
    {
      $model = Provider::find($id);
      // preload category model for provider
      $model->category;
      return $this->success($model);
    }

    public function update(Request $request, int $id)
    {
      $model = Provider::find($id);
      if($model){
        $model->fill($request->all());
        $model->save();
      }
      // preload category model for provider
      $model->category;
      return $this->success($model);
    }

    public function delete(int $id)
    {
      $model = Provider::find($id);
      if($model){
        $model->delete();
      }
      return $this->success();
    }

    public function feeds(int $id)
    {
      $model = Provider::find($id);
      if(!$model){
        return $this->success();
      }

      return $this->success($model->feeds);
    }
}
