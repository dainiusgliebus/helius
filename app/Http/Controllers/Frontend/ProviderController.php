<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Provider;

class ProviderController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index()
  {
      return view('providers');
  }


  public function feeds($id)
  {
    $model = Provider::findOrFail($id);
    return view('providers-feeds', ['provider' => $model]);
  }
}
