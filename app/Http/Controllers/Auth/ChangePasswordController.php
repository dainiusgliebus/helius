<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\User\ChangePasswordRequest;

class ChangePasswordController extends Controller
{

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function formPage(Request $request){

    return view('auth/passwords/change');
  }

  public function formRequest(ChangePasswordRequest $request){

    if($request->password == $request->password_confirmation){
      $user = Auth::user();
      $user->password = Hash::make($request->password);
      $user->save();
      //return redirect()->route('user.password.change')->with('status', 'Password updated!');
      return redirect()->route('home');
    }

    return redirect()->route('user.password.change')->with('warning', 'Password missmatch');
  }

}
