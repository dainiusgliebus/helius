<?php

namespace App\Http\Middleware\Validators;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class BaseValidator
{
  public function validationError($errors, int $error_code = 422) {
    return response()->json([
      'message' => 'failed',
      'errors' => $errors,
    ], $error_code);
  }
}
