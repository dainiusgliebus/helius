<?php

namespace App\Http\Middleware\Validators;

use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use Validator;
use Closure;

class CategoryValidator extends BaseValidator
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle(Request $request, Closure $next)
  {
    $rules = [
      'title'  => 'required|min:3',
    ];

    $validator = Validator::make($request->all(), $rules);

    if($validator->fails()){
      return $this->validationError($validator->errors());
    }

    return $next($request);
  }
}
