<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Hash;
use Illuminate\Console\Command;
use App\User;

class UserCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create admin user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->ask('Please type in username:');
        $password = $this->secret('Please type password:');
        $email = $this->ask('Please type email:');

        $user = new User();
        $user->name = $name;
        $user->password = Hash::make($password);
        $user->email = $email;
        if($user->save()){
          $this->info('User created');
        }else{
          $this->warning('Failed to create user');
        }

    }
}
