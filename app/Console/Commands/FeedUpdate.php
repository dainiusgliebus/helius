<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Provider;
use App\Models\Feed;

class FeedUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pull feeds from providers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $providers = Provider::all();

      foreach($providers as $provider){
        $this->info('Running provider: '.$provider->title);
        $content = file_get_contents($provider->url);

        if($content === false){
          continue;
        }

        $rss = simplexml_load_string($content);

        foreach ($rss->channel->item as $item) {
            $feed = Feed::where('link', $item->link)->first();
            if($feed){
              // skip if exist
              continue;
            }
            $feed = new Feed();
            $feed->title       = $item->title;
            $feed->link        = $item->link;
            $feed->description = $item->description;
            $feed->category    = $item->category;
            $feed->provider_id = $provider->id;
            $feed->save();
        }

      }

    }
}
