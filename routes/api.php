<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::namespace('Api')->group(function () {
  Route::prefix('providers')->name('providers')->group(function () {
    Route::get(   '',           'ProviderController@list')  ->name('list');
    Route::put(   '',           'ProviderController@create')->middleware(['validator.providers'])->name('create');
    Route::get(   '{id}',       'ProviderController@view')  ->name('view');
    Route::post(  '{id}',       'ProviderController@update')->middleware(['validator.providers'])->name('update');
    Route::delete('{id}',       'ProviderController@delete')->name('delete');
    Route::get(   '{id}/feeds', 'ProviderController@feeds')  ->name('feeds');
  });

  Route::prefix('categories')->name('categories')->group(function () {
    Route::get(   '',     'CategoryController@list')  ->name('list');
    Route::put(   '',     'CategoryController@create')->middleware(['validator.categories'])->name('create');
    Route::delete('{id}', 'CategoryController@delete')->name('delete');
  });

  Route::prefix('feeds')->name('feeds')->group(function () {
    Route::get(   '',        'FeedsController@list')  ->name('list');
    Route::get(   '{id}',    'FeedsController@view')  ->name('view');
    Route::delete('{id}',    'FeedsController@delete')->name('delete');
    Route::get('categories', 'FeedsController@categories')  ->name('categories');
  });
});
