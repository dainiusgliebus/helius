<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('feeds');
});

Auth::routes();

Route::get('/home', 'Frontend\FeedsController@index')->name('home');

Route::prefix('password')->name('user.password.')->group(function () {
  Route::get('change', 'Auth\ChangePasswordController@formPage')->name('change');
  Route::post('change', 'Auth\ChangePasswordController@formRequest')->name('change.request');
});

Route::namespace('Frontend')->name('frontend.')->group(function () {
    Route::get('/providers', 'ProviderController@index')->name('providers');
    Route::get('/providers/{id}/feeds', 'ProviderController@feeds')->name('providers.feeds');
    Route::get('/categories', 'CategoryController@index')->name('categories');
    Route::get('/feeds', 'FeedsController@index')->name('feeds');
});
