Diegimas (standartinis):

1. atsiusti is git koda `git clone ******`.
2. paleisti composer setup (nieko nepridejau, tai turetu buti standatinis laravel componentai) `composer install`
3. sukurti faila `/database/database.sqlite` (ar kitoki del duomenu bazes)
4. `.env.example` failo nukopijavimas ir setup (nieko naujo, gal tik, kad `DB_DATABASE` reik nurodyti kelia iki `database.sqlite`)
5. setup db su `php artisan migrate`, taip pat as pridejau kelis "seed" todel darant setup duomenu baze, jeigu noro yra naudoti juos reik seed irgi prideti `php artisan migrate --seed`
6. jei buvo panaudota db seed tada galima iskart pasiimti feeds su `php artisan feed:update`
7. default user and seed yra `admin` su slaptazodziu `1234` ir email `admin@admin.com`
8. nauja user sukurti `php artisan user:create`

Papildomai:

Docker nenaudojau, nes tiesiog ne itin gerai jis draugauja pas mane su mac (ypac, kai reikedavo dirbti su syfmony projektais tai response budavo labai letas).

Projekta padariau ant "Valet" (https://laravel.com/docs/master/valet).

Queues niekur nenaudojau, bet jeigu reik visada feed update galima padaryti kaip job, kuris automiskai sudeda info kokius feed updatinti ir tada queue naudonjant updatina.

Nebuvo nieko parasyta ka daryti del senu feed, tai as darant feed update pridedu naujus feed (netrinu senu).

Naudojau daug kur frontend Jquery.

Dizainas standartinis laravel su trupuciu bootstrap. bet ne per daug... nesu didelis dizaineris, atkartoti dizaina galiu, bet ne kurti pats.

User auth pagrindas padarytas naudojant `php artisan make:auth`, tai tik pridejau password keitima + command sukurti user.
