<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProviderTest extends TestCase
{
    public function testAddProvider()
    {
      $provider = factory(\App\Models\Provider::class)->make();
      $response = $this->json('PUT', '/api/providers', ['title' => $provider->title, 'url' => $provider->url]);

      $response
            ->assertStatus(200)
            ->assertJson([
                'message' => 'success',
                'data'    => $provider->toArray()
            ]);
    }


    public function testLoadProvidersList()
    {
      $providers = factory(\App\Models\Provider::class, 3)->create();

      $response = $this->json('GET', '/api/providers');

      $response
            ->assertStatus(200)
            ->assertJson([
                'message' => 'success',
                'data'    => $providers->toArray()
            ]);
    }

    public function testViewProviders()
    {
      $provider = factory(\App\Models\Provider::class)->create();

      $response = $this->json('GET', '/api/providers/'.$provider->id);

      $response
            ->assertStatus(200)
            ->assertJson([
                'message' => 'success',
                'data' => $provider->toArray()
            ]);
    }

    public function testUpdateProviders()
    {
      $provider = factory(\App\Models\Provider::class)->create();
      $update = ['title' => time(), 'url' => 'http://test.com'];

      $response = $this->json('POST', '/api/providers/'.$provider->id, $update);

      $response
            ->assertStatus(200)
            ->assertJson([
                'message' => 'success',
                'data' => $update
            ]);
    }

    public function testDeleteProvider()
    {
      $provider = factory(\App\Models\Provider::class)->create();

      $response = $this->json('DELETE', '/api/providers/'.$provider->id);

      $response
            ->assertStatus(200)
            ->assertJson([
                'message' => 'success'
            ]);
    }

}
