<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
  public function testAddCategory()
  {
    $model = factory(\App\Models\Category::class)->make();
    $response = $this->json('PUT', '/api/categories', ['title' => $model->title]);

    $response
          ->assertStatus(200)
          ->assertJson([
              'message' => 'success',
              'data'    => $model->toArray()
          ]);
  }


  public function testLoadCategoriesList()
  {
    $model = factory(\App\Models\Category::class, 3)->create();
    $response = $this->json('GET', '/api/categories');

    $response
          ->assertStatus(200)
          ->assertJson([
              'message' => 'success',
              'data'    => $model->toArray()
          ]);
  }


  public function testDeleteCategory()
  {
    $model = factory(\App\Models\Category::class)->create();

    $response = $this->json('DELETE', '/api/categories/'.$model->id);

    $response
          ->assertStatus(200)
          ->assertJson([
              'message' => 'success'
          ]);
  }
}
