<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FeedTest extends TestCase
{

  public function testAddFeed()
  {
    $model = factory(\App\Models\Feed::class)->create();
    $response = $this->json('GET', '/api/feeds/'.$model->id);

    $response
          ->assertStatus(200)
          ->assertJson([
              'message' => 'success',
              'data'    => $model->toArray()
          ]);
  }

  public function testLoadFeedsList()
  {
    $model = factory(\App\Models\Feed::class, 5)->create();
    $response = $this->json('GET', '/api/feeds');

    $response
          ->assertStatus(200)
          ->assertJson([
              'message' => 'success',
              'data'    => $model->toArray()
          ]);
  }


  public function testDeleteFeed()
  {
    $model = factory(\App\Models\Feed::class)->create();

    $response = $this->json('DELETE', '/api/feeds/'.$model->id);

    $response
          ->assertStatus(200)
          ->assertJson([
              'message' => 'success'
          ]);
  }
}
